package com.zhy.magicviewpager.sample;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_btn_standard).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                onStandardViewPager(component);
            }
        });

        findComponentById(ResourceTable.Id_btn_circle).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                onCircleViewPager(component);
            }
        });
    }

    public void onStandardViewPager(Component view) {
        Intent intent = new Intent();
        Operation operation =
                new Intent.OperationBuilder().withBundleName(getBundleName()).withAbilityName(StandardViewPagerAbility.class).build();
        intent.setOperation(operation);
        startAbility(intent);
    }

    public void onCircleViewPager(Component view) {
        Intent intent = new Intent();
        Operation operation =
                new Intent.OperationBuilder().withBundleName(getBundleName()).withAbilityName(CircleViewPagerAbility.class).build();
        intent.setOperation(operation);
        startAbility(intent);
    }
}
