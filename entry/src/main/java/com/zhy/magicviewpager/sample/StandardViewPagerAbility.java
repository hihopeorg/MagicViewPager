package com.zhy.magicviewpager.sample;

import com.zhy.magicviewpager.transformer.AlphaPageTransformer;
import com.zhy.magicviewpager.transformer.NonPageTransformer;
import com.zhy.magicviewpager.transformer.RotateDownPageTransformer;
import com.zhy.magicviewpager.transformer.RotateUpPageTransformer;
import com.zhy.magicviewpager.transformer.ScaleInTransformer;
import com.zhy.magicviewpager.transformer.TransformPageSlider;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.PageSliderProvider;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.PopupDialog;

public class StandardViewPagerAbility extends Ability implements Component.ClickedListener {
    private TransformPageSlider mViewPager;
    private PageSliderProvider mAdapter;

    int[] imgRes = {ResourceTable.Media_a, ResourceTable.Media_b, ResourceTable.Media_c, ResourceTable.Media_d,
            ResourceTable.Media_e, ResourceTable.Media_f, ResourceTable.Media_g, ResourceTable.Media_h,
            ResourceTable.Media_i};

    PopupDialog dialog;
    Text title;
    Text actionSettings;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_standard_view_pager);

        mViewPager = (TransformPageSlider) findComponentById(ResourceTable.Id_id_viewpager);

        mViewPager.setPageMargin(40);
        mViewPager.setPageCacheSize(3);
        mViewPager.setProvider(mAdapter = new PageSliderProvider() {
            @Override
            public Object createPageInContainer(ComponentContainer container, int position) {
                Image view = new Image(StandardViewPagerAbility.this);
                view.setScaleMode(Image.ScaleMode.STRETCH);

                view.setPixelMap(imgRes[position]);
                container.addComponent(view,
                        new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                                ComponentContainer.LayoutConfig.MATCH_PARENT));
                return view;
            }

            @Override
            public void destroyPageFromContainer(ComponentContainer container, int position, Object object) {
                container.removeComponent((Component) object);
            }

            @Override
            public int getCount() {
                return imgRes.length;
            }

            @Override
            public boolean isPageMatchToObject(Component view, Object o) {
                return view == o;
            }
        });
        mViewPager.setPageTransformer(true, new AlphaPageTransformer());

        actionSettings = (Text) findComponentById(ResourceTable.Id_action_settings);
        actionSettings.setClickedListener(this);

        title = (Text) findComponentById(ResourceTable.Id_title);

    }

    @Override
    public void onClick(Component component) {
        if (component.getId() == ResourceTable.Id_action_settings) {
            showActionsDialog();
            return;
        }
        String title = ((Button) component).getText();

        if ("RotateDown".equals(title)) {
            mViewPager.setPageTransformer(true, new RotateDownPageTransformer());
        } else if ("RotateUp".equals(title)) {
            mViewPager.setPageTransformer(true, new RotateUpPageTransformer());
        } else if ("Standard".equals(title)) {
            mViewPager.setPageTransformer(true, NonPageTransformer.INSTANCE);
        } else if ("Alpha".equals(title)) {
            mViewPager.setPageTransformer(true, new AlphaPageTransformer());
        } else if ("ScaleIn".equals(title)) {
            mViewPager.setPageTransformer(true, new ScaleInTransformer());
        } else if ("RotateDown and Alpha".equals(title)) {
            mViewPager.setPageTransformer(true, new RotateDownPageTransformer(new AlphaPageTransformer()));
        } else if ("RotateDown and Alpha And ScaleIn".equals(title)) {
            mViewPager.setPageTransformer(true,
                    new RotateDownPageTransformer(new AlphaPageTransformer(new ScaleInTransformer())));
        }
        setTitle(title);
        if (dialog != null && dialog.isShowing()) {
            dialog.hide();
        }
        dialog = null;
    }

    private void setTitle(String title) {
        this.title.setText(title);
    }

    private void showActionsDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.hide();
        }
        dialog = new PopupDialog(getContext(), actionSettings);
        Component layout = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_menu, null, false);
        layout.findComponentById(ResourceTable.Id_rotate_down).setClickedListener(this);
        layout.findComponentById(ResourceTable.Id_rotate_up).setClickedListener(this);
        layout.findComponentById(ResourceTable.Id_standard).setClickedListener(this);
        layout.findComponentById(ResourceTable.Id_alpha).setClickedListener(this);
        layout.findComponentById(ResourceTable.Id_scale_in).setClickedListener(this);
        layout.findComponentById(ResourceTable.Id_rotate_down_and_alpha).setClickedListener(this);
        layout.findComponentById(ResourceTable.Id_rotate_down_and_alpha_and_scale_in).setClickedListener(this);
        dialog.setCustomComponent(layout).showOnCertainPosition(LayoutAlignment.TOP | LayoutAlignment.RIGHT, 0, 0);
    }
}
