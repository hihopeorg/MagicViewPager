package com.zhy.magicviewpager.sample;

import com.zhy.magicviewpager.sample.util.EventHelper;
import com.zhy.magicviewpager.transformer.TransformPageSlider;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.*;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.IOException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MainAbilityTest {
    private static Ability ability = EventHelper.startAbility(MainAbility.class);
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x01214, "MagicViewTest");

    private void stopThread(int x) {
        try {
            Thread.sleep(x);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void exec(String x) {
        try {
            Runtime.getRuntime().exec(x);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test01RotateDown() {
        Button button = (Button) ability.findComponentById(ResourceTable.Id_btn_standard);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, button);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        Text settings = (Text) currentTopAbility.findComponentById(ResourceTable.Id_action_settings);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, settings);
        stopThread(1000);
        exec("input tap 540 200");
        stopThread(2000);
        TransformPageSlider transformPageSlider = (TransformPageSlider) currentTopAbility.findComponentById(ResourceTable.Id_id_viewpager);
        Image image = (Image) ((StackLayout) ((DirectionalLayout) transformPageSlider.getComponentAt(0)).getComponentAt(0)).getComponentAt(0);
        float rotation = image.getRotation();
        float pivotX = image.getPivotX();
        exec("input swipe 700 1300 100 1300 1000");
        stopThread(1500);
        float rotation1 = image.getRotation();
        float pivotX1 = image.getPivotX();
        Assert.assertTrue("向下旋转效果切换失败", rotation > rotation1 && pivotX < pivotX1);
        stopThread(3000);
        exec("input swipe 700 1300 100 1300 1000");
        stopThread(3000);
        exec("input keyevent 4");
        stopThread(1000);
    }

    @Test
    public void test02RotateUp() {
        Button button = (Button) ability.findComponentById(ResourceTable.Id_btn_standard);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, button);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        Text settings = (Text) currentTopAbility.findComponentById(ResourceTable.Id_action_settings);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, settings);
        stopThread(1000);
        exec("input tap 540 350");
        stopThread(2000);
        TransformPageSlider transformPageSlider = (TransformPageSlider) currentTopAbility.findComponentById(ResourceTable.Id_id_viewpager);
        Image image = (Image) ((StackLayout) ((DirectionalLayout) transformPageSlider.getComponentAt(0)).getComponentAt(0)).getComponentAt(0);
        float rotation = image.getRotation();
        float pivotX = image.getPivotX();
        exec("input swipe 700 1300 100 1300 1000");
        stopThread(1500);
        float rotation1 = image.getRotation();
        float pivotX1 = image.getPivotX();
        Assert.assertTrue("向上旋转效果切换失败", rotation < rotation1 && pivotX < pivotX1);
        stopThread(3000);
        exec("input swipe 700 1300 100 1300 1000");
        stopThread(3000);
        exec("input keyevent 4");
        stopThread(1000);
    }

    @Test
    public void test03standard() {
        Button button = (Button) ability.findComponentById(ResourceTable.Id_btn_standard);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, button);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        Text settings = (Text) currentTopAbility.findComponentById(ResourceTable.Id_action_settings);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, settings);
        stopThread(1000);
        exec("input tap 540 500");
        stopThread(2000);
        TransformPageSlider transformPageSlider = (TransformPageSlider) currentTopAbility.findComponentById(ResourceTable.Id_id_viewpager);
        Image image = (Image) ((StackLayout) ((DirectionalLayout) transformPageSlider.getComponentAt(0)).getComponentAt(0)).getComponentAt(0);
        int width = image.getLocationOnScreen()[0];
        int height = image.getLocationOnScreen()[1];
        exec("input swipe 700 1300 100 1300 1000");
        stopThread(1500);
        int width1 = image.getLocationOnScreen()[0];
        int height1 = image.getLocationOnScreen()[1];
        HiLog.warn(LABEL, width + ";" + width1 + ":" + height + ":" + height1);
        Assert.assertTrue("平移切换效果失败", width1 < width && height == height1);
        stopThread(3000);
        exec("input swipe 700 1300 100 1300 1000");
        stopThread(3000);
        exec("input keyevent 4");
        stopThread(1000);
    }

    @Test
    public void test04alpha() {
        Button button = (Button) ability.findComponentById(ResourceTable.Id_btn_standard);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, button);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        Text settings = (Text) currentTopAbility.findComponentById(ResourceTable.Id_action_settings);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, settings);
        stopThread(1000);
        exec("input tap 540 650");
        stopThread(2000);
        TransformPageSlider transformPageSlider = (TransformPageSlider) currentTopAbility.findComponentById(ResourceTable.Id_id_viewpager);
        Image image = (Image) ((StackLayout) ((DirectionalLayout) transformPageSlider.getComponentAt(0)).getComponentAt(0)).getComponentAt(0);
        float alpha = image.getAlpha();
        exec("input swipe 700 1300 100 1300 1000");
        stopThread(1500);
        float alpha1 = image.getAlpha();
        Assert.assertTrue("透明度效果变化失败", alpha > alpha1);
        stopThread(3000);
        exec("input swipe 700 1300 100 1300 1000");
        stopThread(3000);
        exec("input keyevent 4");
        stopThread(1000);
    }

    @Test
    public void test05ScaleIn() {
        Button button = (Button) ability.findComponentById(ResourceTable.Id_btn_standard);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, button);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        Text settings = (Text) currentTopAbility.findComponentById(ResourceTable.Id_action_settings);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, settings);
        stopThread(1000);
        exec("input tap 540 850");
        stopThread(2000);
        TransformPageSlider transformPageSlider = (TransformPageSlider) currentTopAbility.findComponentById(ResourceTable.Id_id_viewpager);
        Image image = (Image) ((StackLayout) ((DirectionalLayout) transformPageSlider.getComponentAt(0)).getComponentAt(0)).getComponentAt(0);

        float scaleX = image.getScaleX();
        float scaleY = image.getScaleY();
        float pivotX = image.getPivotX();

        exec("input swipe 700 1300 100 1300 1000");
        stopThread(1500);
        float scaleX1 = image.getScaleX();
        float scaleY1 = image.getScaleY();
        float pivotX1 = image.getPivotX();
        Assert.assertTrue("缩放效果变化失败", scaleX > scaleX1 && scaleY > scaleY1 && pivotX < pivotX1);
        stopThread(3000);
        exec("input swipe 700 1300 100 1300 1000");
        stopThread(3000);
        exec("input keyevent 4");
        stopThread(1000);
    }

    @Test
    public void test06RotateDown_Alpha() {
        Button button = (Button) ability.findComponentById(ResourceTable.Id_btn_standard);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, button);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        Text settings = (Text) currentTopAbility.findComponentById(ResourceTable.Id_action_settings);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, settings);
        stopThread(1000);
        exec("input tap 540 1000");
        stopThread(2000);
        TransformPageSlider transformPageSlider = (TransformPageSlider) currentTopAbility.findComponentById(ResourceTable.Id_id_viewpager);
        Image image = (Image) ((StackLayout) ((DirectionalLayout) transformPageSlider.getComponentAt(0)).getComponentAt(0)).getComponentAt(0);
        float alpha = image.getAlpha();
        float rotation = image.getRotation();
        float pivotX = image.getPivotX();
        exec("input swipe 700 1300 100 1300 1000");
        stopThread(1500);
        float alpha1 = image.getAlpha();
        float rotation1 = image.getRotation();
        float pivotX1 = image.getPivotX();
        Assert.assertTrue("向下旋转且透明效果失败", rotation > rotation1 && pivotX < pivotX1 && alpha > alpha1);
        stopThread(3000);
        exec("input swipe 700 1300 100 1300 1000");
        stopThread(3000);
        exec("input keyevent 4");
        stopThread(1000);
    }

    @Test
    public void test07RotateDown_Alpha_ScaleIn() {
        Button button = (Button) ability.findComponentById(ResourceTable.Id_btn_standard);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, button);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        Text settings = (Text) currentTopAbility.findComponentById(ResourceTable.Id_action_settings);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, settings);
        stopThread(1000);
        exec("input tap 540 1200");
        stopThread(2000);
        TransformPageSlider transformPageSlider = (TransformPageSlider) currentTopAbility.findComponentById(ResourceTable.Id_id_viewpager);
        Image image = (Image) ((StackLayout) ((DirectionalLayout) transformPageSlider.getComponentAt(0)).getComponentAt(0)).getComponentAt(0);
        float alpha = image.getAlpha();
        float rotation = image.getRotation();
        float pivotX = image.getPivotX();
        float scaleX = image.getScaleX();
        float scaleY = image.getScaleY();
        exec("input swipe 700 1300 100 1300 1000");
        stopThread(1500);
        float alpha1 = image.getAlpha();
        float rotation1 = image.getRotation();
        float pivotX1 = image.getPivotX();
        float scaleX1 = image.getScaleX();
        float scaleY1 = image.getScaleY();
        Assert.assertTrue("向下旋转且透明效果失败", rotation > rotation1 && pivotX < pivotX1 && alpha > alpha1 && scaleX > scaleX1 && scaleY > scaleY1);
        stopThread(3000);
        exec("input swipe 700 1300 100 1300 1000");
        stopThread(3000);
        exec("input keyevent 4");
        stopThread(1000);
    }

    @Test
    public void test08Circle() {
        Button button = (Button) ability.findComponentById(ResourceTable.Id_btn_circle);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, button);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        stopThread(1000);
        TransformPageSlider transformPageSlider = (TransformPageSlider) currentTopAbility.findComponentById(ResourceTable.Id_id_viewpager);
        int currentPage = transformPageSlider.getCurrentPage();
        int currentPage1 = -1;
        int times = 0;
        do {
            exec("input swipe 300 1300 1000 1300 1000");
            stopThread(3000);
            currentPage1 = transformPageSlider.getCurrentPage();
            times++;
        } while (currentPage != currentPage1 && times < 30);

        Assert.assertTrue("图片未能循环", currentPage == currentPage1);
        exec("input keyevent 4");
        stopThread(1000);
    }

}