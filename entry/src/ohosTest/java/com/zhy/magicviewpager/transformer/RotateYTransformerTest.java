package com.zhy.magicviewpager.transformer;

import ohos.agp.components.Component;
import org.junit.Test;

import static com.zhy.magicviewpager.transformer.AlphaPageTransformerTest.context;
import static org.junit.Assert.*;

public class RotateYTransformerTest {

    @Test
    public void pageTransform() {
        RotateYTransformer transformer = new RotateYTransformer();
        Component component = new Component(context);
        transformer.pageTransform(component,0);
        assertEquals(0.0f,component.getPivotX(),0.0);
    }
}