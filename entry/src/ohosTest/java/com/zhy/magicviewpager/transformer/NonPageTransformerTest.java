package com.zhy.magicviewpager.transformer;

import ohos.agp.components.Component;
import org.junit.Test;

import static com.zhy.magicviewpager.transformer.AlphaPageTransformerTest.context;
import static org.junit.Assert.*;

public class NonPageTransformerTest {

    @Test
    public void transformPage() {
        NonPageTransformer transformer = new NonPageTransformer();
        Component component = new Component(context);
        transformer.transformPage(component,0);
        assertEquals(0.999f,component.getScaleX(),0.0);
    }
}