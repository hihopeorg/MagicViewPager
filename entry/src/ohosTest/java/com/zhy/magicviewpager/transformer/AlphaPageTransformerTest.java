package com.zhy.magicviewpager.transformer;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Component;
import ohos.app.Context;
import org.junit.Test;

import static org.junit.Assert.*;

public class AlphaPageTransformerTest {
    public static Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();

    @Test
    public void pageTransform() {
        AlphaPageTransformer transformer = new AlphaPageTransformer();
        Component component = new Component(context);
        transformer.pageTransform(component,0);
        assertEquals(1,component.getAlpha(),0.0);
    }
}