package com.zhy.magicviewpager.transformer;

import ohos.agp.components.Component;
import org.junit.Test;

import static com.zhy.magicviewpager.transformer.AlphaPageTransformerTest.context;
import static org.junit.Assert.*;

public class ScaleInTransformerTest {

    @Test
    public void pageTransform() {
        ScaleInTransformer transformer = new ScaleInTransformer();
        Component component = new Component(context);
        transformer.pageTransform(component,0);
        assertEquals(0,component.getPivotX(),0.0);
    }
}