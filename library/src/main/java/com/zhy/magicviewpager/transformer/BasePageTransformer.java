package com.zhy.magicviewpager.transformer;

import ohos.agp.components.Component;

/**
 * Created by zhy on 16/5/7.
 */
public abstract class BasePageTransformer implements TransformPageSlider.PageTransformer
{
    protected TransformPageSlider.PageTransformer mPageTransformer = NonPageTransformer.INSTANCE;
    public static final float DEFAULT_CENTER = 0.5f;

    public void transformPage(Component view, float position)
    {
        if (mPageTransformer != null)
        {
            mPageTransformer.transformPage(view, position);
        }

        pageTransform(view, position);
    }

    protected abstract void pageTransform(Component view, float position);


}
