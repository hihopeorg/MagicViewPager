package com.zhy.magicviewpager.transformer;

import ohos.agp.components.Component;

/**
 * Created by zhy on 16/5/7.
 */
public class NonPageTransformer implements TransformPageSlider.PageTransformer
{
    @Override
    public void transformPage(Component page, float position)
    {
        page.setScaleX(0.999f);//hack
    }

    public static final TransformPageSlider.PageTransformer INSTANCE = new NonPageTransformer();
}
