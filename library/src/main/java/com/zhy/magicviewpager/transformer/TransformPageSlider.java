/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zhy.magicviewpager.transformer;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.PageSlider;
import ohos.agp.components.PageSliderProvider;
import ohos.agp.components.StackLayout;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Subclass of {@link PageSlider}. Provides a {@link PageTransformer} interface, it can apply a property
 * transformation to the given page while sliding. And use {@link #setPageTransformer} method to set it.
 */
public class TransformPageSlider extends PageSlider {
    /**
     * A PageTransformer is invoked whenever a visible/attached page is scrolled.
     * This offers an opportunity for the application to apply a custom transformation
     * to the page views using animation properties.
     */
    public interface PageTransformer {
        /**
         * Apply a property transformation to the given page.
         *
         * @param page     Apply the transformation to this page
         * @param position Position of page relative to the current front-and-center
         *                 position of the pager. 0 is front and center. 1 is one full
         *                 page position to the right, and -1 is one page position to the left.
         */
        void transformPage(Component page, float position);
    }

    private PageSliderProviderWrapper pageSliderProviderWrapper = null;
    private final PageChangedListenerWrapper pageChangedListenerWrapper = new PageChangedListenerWrapper();
    private boolean reverse = false;
    private PageTransformer pageTransformer;
    private Component[] toBeTransform = new Component[3];

    private boolean firstDragAfterSetCircle;

    public TransformPageSlider(Context context) {
        super(context);
        init();
    }

    public TransformPageSlider(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init();
    }

    public TransformPageSlider(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    private void init() {
        super.addPageChangedListener(pageChangedListenerWrapper);
    }

    /**
     * Sets a {@link PageTransformer} that will be called for each attached page whenever
     * the scroll position is changed. This allows the application to apply custom property
     * transformations to each page, overriding the default sliding behavior.
     *
     * @param reverse         true if the supplied PageTransformer requires page views
     *                        to be drawn from last to first instead of first to last.
     * @param pageTransformer PageTransformer that will modify each page's animation properties
     */
    public final void setPageTransformer(boolean reverse, PageTransformer pageTransformer) {
        clearTransformEffects();
        this.reverse = reverse;
        this.pageTransformer = pageTransformer;
        refreshPagesToTransformByOrder((ComponentContainer) getComponentAt(0), reverse);
        transformPages(getCurrentPage(), 0);
    }

    @Override
    public final void setProvider(PageSliderProvider provider) {
        if (pageSliderProviderWrapper == null) {
            pageSliderProviderWrapper = new PageSliderProviderWrapper(provider);
        } else {
            pageSliderProviderWrapper.setPageSliderProvider(provider);
        }
        super.setProvider(pageSliderProviderWrapper);
    }

    @Override
    public final void addPageChangedListener(PageChangedListener listener) {
        pageChangedListenerWrapper.addPageChangedListener(listener);
    }

    @Override
    public final void removePageChangedListener(PageChangedListener listener) {
        pageChangedListenerWrapper.removePageChangedListener(listener);
    }

    public final void clearPageChangedListeners() {
        pageChangedListenerWrapper.clearPageChangedListeners();
    }

    @Override
    public void setCircularModeEnabled(boolean enabled) {
        super.setCircularModeEnabled(enabled);
        firstDragAfterSetCircle = true;
    }

    private class PageChangedListenerWrapper implements PageChangedListener {
        private List<PageChangedListener> pageChangedListeners = new ArrayList<>();

        public void addPageChangedListener(PageChangedListener pageChangedListener) {
            if (!pageChangedListeners.contains(pageChangedListener)) {
                pageChangedListeners.add(pageChangedListener);
            }
        }

        public void removePageChangedListener(PageChangedListener pageChangedListener) {
            pageChangedListeners.remove(pageChangedListener);
        }

        public void clearPageChangedListeners() {
            pageChangedListeners.clear();
        }

        @Override
        public void onPageSliding(int itemPos, float itemPosOffset, int itemPosOffsetPixels) {
            for (int i = pageChangedListeners.size() - 1; i >= 0; i--) {
                pageChangedListeners.get(i).onPageSliding(itemPos, itemPosOffset, itemPosOffsetPixels);
            }
            if (itemPosOffsetPixels == 0) {
                transformPages(getCurrentPage(), 0);
                return;
            }
            switch (TransformPageSlider.this.getCurrentSlidingState()) {
                case PageSlider.SLIDING_STATE_DRAGGING:
                    if (firstDragAfterSetCircle) {
                        // workaround for first drag in circle mode, itemPosOffset is a strange number which > 1
                        int pageWidth = toBeTransform[1].getWidth() + getPageMargin();
                        transformPages(itemPos,
                                (getCachedPagesLimit() * pageWidth - itemPosOffsetPixels) * 1f / pageWidth);
                    } else {
                        transformPages(itemPos, itemPosOffsetPixels > 0 ? -itemPosOffset : itemPosOffset);
                    }
                    break;
                case PageSlider.SLIDING_STATE_SETTLING:
                    transformPages(itemPos, itemPosOffsetPixels > 0 ? -itemPosOffset : itemPosOffset);
                    // pass-through
                case PageSlider.SLIDING_STATE_IDLE:
                    firstDragAfterSetCircle = false;
                    break;
                default:
                    break;
            }
        }

        @Override
        public void onPageSlideStateChanged(int state) {
            if (state == 0) {
                refreshPagesToTransformByOrder((ComponentContainer) getComponentAt(0), reverse);
            }
            for (int i = pageChangedListeners.size() - 1; i >= 0; i--) {
                pageChangedListeners.get(i).onPageSlideStateChanged(state);
            }
        }

        @Override
        public void onPageChosen(int itemPos) {
            for (int i = pageChangedListeners.size() - 1; i >= 0; i--) {
                pageChangedListeners.get(i).onPageChosen(itemPos);
            }
        }
    }

    private class PageSliderProviderWrapper extends PageSliderProvider {
        private PageSliderProvider userPageSliderProvider;

        public PageSliderProviderWrapper(PageSliderProvider userPageSliderProvider) {
            this.userPageSliderProvider = userPageSliderProvider;
        }

        public void setPageSliderProvider(PageSliderProvider userPageSliderProvider) {
            this.userPageSliderProvider = userPageSliderProvider;
        }

        @Override
        public int getCount() {
            return userPageSliderProvider == null ? 0 : userPageSliderProvider.getCount();
        }

        @Override
        public Object createPageInContainer(ComponentContainer componentContainer, int i) {
            if (userPageSliderProvider == null) {
                return null;
            }
            Object newPage = userPageSliderProvider.createPageInContainer(componentContainer, i);
            if (newPage instanceof Component) {
                ((Component) newPage).setClipEnabled(false);
            }
            componentContainer.setTag(i);
            return newPage;
        }

        @Override
        public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
            if (userPageSliderProvider == null) {
                return;
            }
            Component page = componentContainer.getComponentAt(0);
            if (page != null && pageTransformer != null) {
                removeTransition(page);
            }
            userPageSliderProvider.destroyPageFromContainer(componentContainer, i, o);
            componentContainer.setTag(null);
        }

        @Override
        public boolean isPageMatchToObject(Component component, Object o) {
            return userPageSliderProvider == null ? false : userPageSliderProvider.isPageMatchToObject(component, o);
        }

        @Override
        public void startUpdate(ComponentContainer container) {
            if (userPageSliderProvider != null) {
                userPageSliderProvider.startUpdate(container);
            }
        }

        @Override
        public void onUpdateFinished(ComponentContainer componentContainer) {
            if (userPageSliderProvider != null) {
                userPageSliderProvider.onUpdateFinished(componentContainer);
            }
        }

        @Override
        public int getPageIndex(Object object) {
            return userPageSliderProvider == null ? super.getPageIndex(object) :
                    userPageSliderProvider.getPageIndex(object);
        }

        @Override
        public void notifyDataChanged() {
            if (userPageSliderProvider != null) {
                userPageSliderProvider.notifyDataChanged();
            }
        }

        @Override
        public String getPageTitle(int position) {
            return userPageSliderProvider == null ? super.getPageTitle(position) :
                    userPageSliderProvider.getPageTitle(position);
        }
    }

    private void transformPages(int currentIndex, float baseOffset) {
        if (pageTransformer == null) {
            return;
        }
        Component directionalLayout = getComponentAt(0);
        if (!(directionalLayout instanceof DirectionalLayout)) {
            // null or other component, it means ohos changes implementation of PageSlider.
            return;
        }
        for (int i = 0; i < toBeTransform.length; i++) {
            Component component = toBeTransform[i];
            if (component == null) {
                continue;
            }
            pageTransformer.transformPage(((StackLayout) component).getComponentAt(0),
                    (reverse ? (1 - i) : (i - 1)) + baseOffset);
        }
    }

    private void clearTransformEffects() {
        Component directionalLayout = getComponentAt(0);
        if (!(directionalLayout instanceof DirectionalLayout)) {
            // null or other component, it means ohos changes implementation of PageSlider.
            return;
        }
        int childCount = ((DirectionalLayout) directionalLayout).getChildCount();
        for (int i = 0; i < childCount; i++) {
            Component component = ((DirectionalLayout) directionalLayout).getComponentAt(i);
            if (component instanceof StackLayout && component.getTag() instanceof Integer) {
                removeTransition(((StackLayout) component).getComponentAt(0));
            }
        }
    }

    // select 3 component at most (current - 1, current, current + 1)
    private void refreshPagesToTransformByOrder(ComponentContainer container, boolean reverse) {
        Arrays.fill(toBeTransform, null);
        int count = pageSliderProviderWrapper == null ? 0 : pageSliderProviderWrapper.getCount();
        if (count == 0) {
            return;
        }

        int currentIndex = getCurrentPage();
        for (int i = 0; i < container.getChildCount(); i++) {
            StackLayout component = (StackLayout) container.getComponentAt(i);
            if (component.getChildCount() > 0 && component.getTag() instanceof Integer) {
                int tag = (Integer) component.getTag();
                if (Math.abs(tag - currentIndex) <= 1 || (isCircularModeEnabled() && (tag * currentIndex == 0) && (tag + currentIndex == count - 1))) {
                    int index = tag - currentIndex + 1;
                    if (index < 0) {
                        index = 2;
                    }
                    if (index > 2) {
                        index = 0;
                    }
                    toBeTransform[index] = component;
                }
            }
        }
        if (reverse) {
            Component tmp = toBeTransform[0];
            toBeTransform[0] = toBeTransform[2];
            toBeTransform[2] = tmp;
        }
    }

    private void removeTransition(Component component) {
        if (component != null) {
            component.setAlpha(1);
            component.setScale(1, 1);
            component.setTranslation(0, 0);
            component.setRotation(0);
        }
    }
}
