# MagicViewPager

**本项目是基于开源项目MagicViewPager进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/hongyangAndroid/MagicViewPager ）追踪到原项目版本**

#### 项目介绍

- 项目名称：MagicViewPager自定义PageSlider库
- 所属系列：ohos的第三方组件适配移植
- 功能：提供PageSlider炫酷切换效果，适用于Banner等
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/hongyangAndroid/MagicViewPager
- 原项目基线版本：未发布Release版本
- 编程语言：Java
- 外部库依赖：无

#### 效果展示 

<img src="gif/MagicViewPager1.gif"/>

#### 安装教程

##### 方案一：

1. 下载MagicViewPager三方库har包library.har。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在module级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```groovy
repositories {
    flatDir { dirs 'libs' }
}
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation(name: 'library', ext: 'har')
	……
}
```

##### 方案二：

  1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址:

```
 repositories {
     maven {
         url 'http://106.15.92.248:8081/repository/Releases/'
     }
 }
```

  2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
 dependencies {
     implementation 'com.zhy.ohos:magic-viewpager:1.0.0'
 }
```

#### 使用说明

### 代码

```java
mViewPager.setPageMargin(40);//设置page间间距，自行根据需求设置
mViewPager.setPageCacheSize(3);//>=3，建议cacheSize * 2 + 1 不要大于总page数
mViewPager.setProvider...//写法不变

//setPageTransformer 决定动画效果
mViewPager.setPageTransformer(true, new RotateDownPageTransformer());
```

##目前可选动画

* AlphaPageTransformer
* RotateDownPageTransformer
* RotateUpPageTransformer
* RotateYTransformer
* NonPageTransformer
* ScaleInTransformer


动画间可以自由组合，例如：

```
mViewPager.setPageTransformer(true, 
	new RotateDownPageTransformer(new AlphaPageTransformer(new ScaleInTransformer())));
```

### 布局

参考如下示例

```xml
    <StackLayout
        ohos:height="160vp"
        ohos:width="match_parent"
        ohos:background_element="#aadc71ff"
        ohos:center_in_parent="true"
        >

        <com.zhy.magicviewpager.transformer.TransformPageSlider
            ohos:id="$+id:id_viewpager"
            ohos:height="120vp"
            ohos:width="match_parent"
            ohos:layout_alignment="center"
            ohos:left_margin="60vp"
            ohos:right_margin="60vp"
            />

    </StackLayout>
```

注意外层`ohos:center_in_parent="true"`.

#### 版本迭代


- v1.0.0
  - 已实现功能：
    通过自定义PageSlider类，支持滑动时page页动画效果设置
    支持多种滑动切换效果，如透明度、缩放、z轴方向旋转
  - 未实现功能：
    受ohos接口限制，暂不支持x轴/y轴方向旋转效果
    受ohos PageSlider特性限制，暂不支持滑动停止后同时显示多页


#### 版权和许可信息

```
Apache License 2.0
```